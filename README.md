**UX Research Overview**

**Hi, Oi, Hola** 👋

This is our research repo, we are part of the [UX Team](https://gitlab.torproject.org/tpo/ux/team) - there you can find more information about how we communicate, and what projects we are working on right now.

If you want to collaborate with our research, please make sure you read:
* [Guidelines do run UR with Tor](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/community/guidelines-ur.md)
* [How to run UR with Tor](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/community/how-to-ur.md)
* [How to report](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/community/user_research_reporting.md)

Here you can find: 
* [past and current activities](https://gitlab.torproject.org/tpo/ux/research/-/tree/master/scripts%20and%20activities)
* [reports from our past activities](https://gitlab.torproject.org/tpo/ux/research/-/tree/master/reports)
* [personas](https://gitlab.torproject.org/tpo/ux/research/-/tree/master/persona)