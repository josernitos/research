###### The Tor Project - UX Team - Q3Q42020 - Anticensorship
---
# Activity: Get Bridges

### Suggested Time
25-45 Minutes

### Materials Needed
Computer, Internet connection
> If you have a VPN activated, please turn it OFF for this test

### Participants
Security trainers, privacy advocates, people we're designing for

### Objective
Find and understand users pain-points during the current bridge adquistion flow and learn about possible opportunities to offer information to educate users in context.

> **pain-point:** anything that prevents users from accomplishing their goals or getting the task done.

### How to do it
1. Open a text file and copy all the questions. You can use https://pad.riseup.net/ to have a shareable pad.
2. Run each of the activities in order and write down how your process was. Any idea or thought that came out during the process can be written down too.
3. Remember: we are not testing you, we are testing our products. All your feedback is appreciated.


### Activity

#### Step 1: Find Tor Browser
Use your regular search engine to find Tor Browser

Q1 Did you find Tor Browser?
[  ] Yes [  ] No

Q2 Can you tell us if the software you are about to download is trustable?
[  ] Yes [  ] No

Q2.1 How did you make that decision?

#### Step 2: Install Tor Browser
Install Tor Browser in your personal device

Q1 Did you have any issue installing Tor Browser?
[  ] Yes [  ] No

Q1.1 If Yes, please explain

Q2 Did you verify Tor Browser signatures?
[  ] Yes [  ] No

Q2.1 From 1 to 5, how complicated was that process for you?

	[1] Not complicated at all
	[2] Somehow complicated
	[3] I don't know what im doing, but i suceed
	[4] I don't know what I'm doing, and I cannot find any information about how to suceed
	[5] I cannot verify Tor Browser signatures

#### Step 3: Connect to Tor
Use Tor Browser to connecting to the Tor network

Q1. Can you connect to the Tor network?
[  ] Yes [  ] No

Q1.1 If not, what did happen? 

#### Step 4: Find Bridges
If you cannot connect directly, try to find a Tor Bridge to help you

Q1. Do you know what is a bridge and how it can help you? Can you explain?

Q2. Did you find a useful bridge?
[  ] Yes [  ] No

Q3. How did you find a useful bridge?

#### Step 5: Get connected to the Tor Network
Use a Bridge in Tor Browser to connecting to the Tor network

Q1. Can you connect to the Tor Network?
[  ] Yes [  ] No

Q1.1 If not, what did happen? 

Q1.2 If yes, are you using a bridge? Which type?


### Report Findings

 - We usually run this kind of activities with groups and in person. Given the COVID19 situation, we are relying on trusted community members to run this activities remotely and individually.
 - For sharing the results safely via Tor you can use [OnionShare](https:///onionshare.org).
 - Please fill our [demographics survey](https://gitlab.torproject.org/tpo/ux/research/-/blob/master/scripts%20and%20activities/2020/user_demographics-en.md) and share it with us, it will help us to understand your context.
 - If you couldn't connect to Tor, you can paste your results in an email to nah@torproject.org or antonela@torproject.org.
 - Encrypting is caring. You can encrypt your [email using PGP](https://www.torproject.org/static/keys/antonela.txt). Remember to attach your own key.

### Report Comments, Concerns and Ideas
If during this activity you find any problem, you got any concern or any idea came to your mind about how to improve this flow, please contact us directly at antonela@torproject.org
