Do you currently use any Tor related product?

- [ ] Yes 
- [ ] No 
- [ ] What is Tor?

Can you select some of them?

- [ ] Tor Browser Desktop 
- [ ] Onion Browser (iOS) 
- [ ] Tor Browser for Android
- [ ] OnionShare
- [ ] Other: (please speficy)

How often do you use Tor Browser?

- [ ] Daily 
- [ ] Weekly 
- [ ] Just in specific situations

Would you describe yourself mostly as a...

- [ ] Day to day technology user
- [ ] Technologists that has a good understanding of tools and how things works
- [ ] Technical Expert
- [ ] Other:

How much do you believe you need protection for your privacy and security online?

From 1 to 5. Understand **1 - Not at all**  and **5 - I'm sure I need it**.
- [ ] 1
- [ ] 2
- [ ] 3
- [ ] 4
- [ ] 5

How old are you?
- [ ] < 20 (less than 20) 
- [ ] Between 20-40y 
- [ ] > 40 (more than 40)

I define my gender as...

- [ ] Female 
- [ ] Male 
- [ ] Other 
- [ ] Prefer not to disclose