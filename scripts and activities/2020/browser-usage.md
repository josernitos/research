## INTRO

**REF:**
* https://lists.torproject.org/pipermail/ux/2019-August/000455.html
* https://gitlab.torproject.org/tpo/ux/research/-/issues/7
* https://gitlab.torproject.org/tpo/ux/research/-/issues/11

This is user research to quantitative measure Tor Browser user's demographics, pain-points, and requested features. The main goals are as following:

- **get to know our users**
- diversity
- frequency of use

- **what do people need**
- what they use our product for
- what are their main pain-points when using TB

**Audience:** Tor Browser users

**Duration:** Q42020 - Q12021

**Recruitment:** We want to survey all Tor Browser users who opt-in by clicking on our survey link in `about:tor`.

**Methodology:** 

This is a pilot exploratory study of discovery and learning. This type of research allows us to explore and learn about the uses of TB, challenge our assumptions, and more deeply understand the people we are designing for.

As pilot research, we ran a printed survey during our Stockholm meeting. Details about [the questions and results](https://lists.torproject.org/pipermail/ux/2019-August/000455.html) were shared in our mailing list.

For this user research, we want to recruit users from `about:tor` in Tor Browser.

**Experience Sample:** is a research method that integrates qualitative, rich data with quantitative, numerical data. Usually runs in-person with a bunch of iterations. In this online study, we will:

- **run the first iteration**
- ask for people to leave contact (e-mail) in case they want to participate in an interview

- **run the second iteration**
- ask if they participated in the first iteration
- ask for people to leave contact (e-mail) in case they want to participate in an interview

- **run interview**
- online interview with those who completed the first and second experience

Our experience sample will be run through an online survey, send it to our communication channel, and be opened for XX weeks. 

We want to collect as many responses as it is possible, they will be analyzed and shared on our private channels. We will select short-list participants for interviews through an optional e-mail contact form. 

**Survey Script**


## WELCOME MSG

Hello!

Thank you so much for participating in this study.
Our main goal is to get to know our user and their needs while browsing through Tor. 

We will ask some questions about you and your use of Tor Browser, and optionally you can share your e-mail for a follow-up interview with our researchers. 

Most questions will ask your experience during browsing, and a good answer is one that you can understand, that is detailed and that does not include one or two words, but at least a full sentence.

Have fun!

## [Demographics](https://gitlab.torproject.org/tpo/ux/research/-/issues/6)
We can use our current demographics - just add this as 1st step in this research

## Experience

1. How often do you use the Tor Browser?
- [ ] This is my first time using the Tor Browser.
- [ ] *PROMPT: What was the reason you recently downloaded Tor Browser?*
- [ ] Always, or very nearly always -- it's my primary browser.
- [ ] Every day.
- [ ] Not every day, but every week.
- [ ] Not every week, but every month.
- [ ] Very occasionally.
- [ ] *PROMPT: What is the reason you don't use Tor Browser more often?*
  
2. Why do you use the Tor Browser, generally speaking?
- [ ] personal privacy
- [ ] work needs
- [ ] staying safe for activism
- [ ] censorship circumvention
- [ ] ideological (supporting Tor, opposing mass surveillance)
- [ ] other reasons. please specify ________.

3. What was the most frustrating thing that happened to you the last time while using the Tor Browser?

4. What things do you like about Tor Browser?

4. What do you NOT like about Tor Browser? Please be specific.

5. If you could make three improvements to Tor Browser, what would they be?

6. (Optional) Please give any additional comments describing your experience using Tor Browser.

## Good Bye Message

Thank you so much for participating in our study!

If you want to be contacted to give us an interview about your uses of Tor Browser, please leave your e-mail here _____. Otherwise, just click in continue to send us your responses.

Thank you!