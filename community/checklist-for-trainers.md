# Checklist for trainers

## Before the training

#### Get prepared!
Please make sure you're prepared for your Tor training.
We have a checklist to make it easy for you.

- [ ] I read and agree with the [Tor Code of Coduct](https://link)
- [ ] I read the [Facilitator Guidelines](https://link)
- [ ] I have a script for the training

#### Security protocol
- [ ] I shared a contact to whom you should call if something happens

#### About the venue
- [ ] The venue has tables and chairs to everyone
- [ ] The venue has internet connection [^1]
- [ ] It has a projector available

#### Audience & Communication
- [ ] I sent e-mail communicating the address and time to participants
- [ ] I asked them to bring the necessary equipament [^2] to the training
- [ ] I have a confirmed audience to the training

[^1]: If you're going to install Tor Browser, make sure the venue has Internet connection available to everyone.
[^2]: If you're going to make a training to install Tor Browser for Desktop, make sure people know they can bring their computer to install it together. It works the other way round for Tor Browser for Android.

## During the training

Before starting the training, make sure you:

- [ ] Make an agreement of no photo disclaimer[^1]
- [ ] Present the agenda
- [ ] Introduce yourself and ask people to introduce themselves
- [ ] "All questions are welcomed"

[^1]: If you're going to take pictures, let the participants it. We don't recommend to post pictures on social media, unless it is an agreement between you and your audience.

During the training:
* Be present
* Give space for people to ask their doubts
* Listen to the questions and write them down with you don't know how to answer it

## After the training

- [ ] Collect feedback
- [ ] Leave an e-mail for further contact and support
- [ ] Make an evaluation about the training
- [ ] Report to Tor
